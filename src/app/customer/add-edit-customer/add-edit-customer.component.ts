import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from "primeng/components/common/messageservice";
import { TranslateService } from '@ngx-translate/core';

// import {Message} from "primeng/components/common/message";
// import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
    selector: 'app-add-edit-customer',
    templateUrl: './add-edit-customer.component.html',
    styleUrls: ['./add-edit-customer.component.css']
})
export class AddEditCustomerComponent implements OnInit, OnChanges {
    newCustomer: any;
    customerTypes: any[];
    governorates: any[];
    areas: any[];
    streets: any[];
    phoneTypes: any[];
    blocks: any[];
    cornerMessage: any = [];
    @Input() existedCustomerData: any = {};
    toggleLoadingLocation: boolean;
    // customerId: any;
    @Output() customerIdValue = new EventEmitter();
    @Output() closeFormNow = new EventEmitter();
    locationObj: any = [];
    collapsible: boolean;

    constructor(private lookup: LookupService, private messageService: MessageService, private translate: TranslateService) {
    }

    ngOnInit() {
        this.newCustomer = {
            locations: [{}],
            customerPhoneBook: [{}]
        };

        console.log(this.existedCustomerData);
        if (this.existedCustomerData) {
            // this.toggleLoadingLocation = true;
            this.newCustomer = this.existedCustomerData;
            // this.getMultipleLocations(0, this.locationObj) 
            console.log(this.newCustomer);
            this.getLocationByCustId(0);
        }

        this.getGovernorates();
    }

    ngOnChanges() {
        console.log(this.existedCustomerData);
        if (this.existedCustomerData) {
            // this.toggleLoadingLocation = true;
            this.newCustomer = this.existedCustomerData;
            // this.getMultipleLocations(0, this.locationObj) 
            console.log(this.newCustomer);
            this.getLocationByCustId(0);
        }
    }

    getLocationByCustId(i) {
        this.toggleLoadingLocation = true;
        this.lookup.getLocationByCustomerId(this.existedCustomerData.id).subscribe(result => {
            this.locationObj = result;
            console.log(JSON.stringify(this.locationObj))
            if (i == this.locationObj.length) {
                console.log('recursive done!');
                this.toggleLoadingLocation = false;
            } else {
                this.getGovernorates().then(() => {
                    console.log('in gov success promise');
                    debugger;
                    this.getAreas(this.locationObj[i].governorate).then(() => {
                        console.log('in area success promise');
                        debugger;
                        this.getBlocks(this.locationObj[i]).then(() => {
                            debugger;
                            console.log('in blocks success promise');
                            this.getStreets(this.locationObj[i]).then(() => {
                                this.getMultipleLocations(++i, this.locationObj)
                            })
                        })
                    })
                });
                this.toggleLoadingLocation = false;
            }
            console.log('loc' + JSON.stringify(this.locationObj))
        }, error => {
            console.log('error')
        })
    }

    getMultipleLocations(i, locations) {
        console.log(i);
        console.log(locations.length);
        if (i == locations.length) {
            console.log('recursive done!');
            // this.toggleLoadingLocation = false;
        } else {
            this.getGovernorates().then(() => {
                console.log('in gov success promise');
                this.getAreas(this.locationObj[i].governorate).then(() => {
                    console.log('in area success promise');
                    this.getBlocks(this.locationObj[i]).then(() => {
                        console.log('in blocks success promise');
                        this.getStreets(this.locationObj[i]).then(() => {
                            this.getMultipleLocations(++i, locations)
                        })
                    })
                })
            });
        }
    }

    addNewLocation() {
        this.locationObj.push({ isNewLocation: true });
    }

    // addNewPhone() {
    //     this.newCustomer.customerPhoneBook.push({});
    //     console.log(this.newCustomer.customerPhoneBook);
    // }

    // removePhone(index) {
    //     console.log(index);
    //     this.newCustomer.customerPhoneBook.splice(index, 1);
    // }

    saveLocation(loc) {
        console.log(loc);
        let locationToPost = {
            "PACINumber": loc.paciNumber,
            "governorate": loc.governorate,
            "area": loc.area,
            "block": loc.block,
            "street": loc.street,
            "title": loc.title,
            "addressNote": loc.addressNote,
            "fk_Customer_Id": this.existedCustomerData.id,
            "id": loc.id
        };
        console.log(locationToPost);
        if (loc.isNewLocation) {
            this.lookup.postNewLocation(locationToPost).subscribe(() => {
                window.scroll(0, 0)
                console.log('success');
                this.messageService.add({
                    severity: "success",
                    summary: this.translate.instant('MSG.sucesful'),
                    detail: this.translate.instant('MSG.locAdSuces')
                });
            },
                err => {
                    window.scroll(0, 0);
                    this.messageService.add({
                        severity: 'error',
                        summary: this.translate.instant('MSG.fail'),
                        detail: this.translate.instant('MSG.failGnratOrdr')
                    })
                })
        } else {
            this.lookup.updateLocation(locationToPost).subscribe(() => {
                console.log('success');
                window.scroll(0, 0)
                this.messageService.add({
                    severity: "success",
                    summary: this.translate.instant('MSG.sucesful'),
                    detail: this.translate.instant('MSG.locSavSuces')
                });
                this.collapsible = false;
            },
                err => {
                    window.scroll(0, 0);
                    this.messageService.add({
                        severity: 'error',
                        summary: this.translate.instant('MSG.fail'),
                        detail: this.translate.instant('MSG.failGnratOrdr')
                    })
                })
        }
    }

    // saveCustomerInfo(info) {
    //     console.log(info);
    //     let customerPhoneBookToPost = [];
    //     info.customerPhoneBook.map((singlePhoneBook) => {
    //         customerPhoneBookToPost.push({
    //             // id: info.id,
    //             phone: singlePhoneBook.phone,
    //             fK_PhoneType_Id: singlePhoneBook.fK_PhoneType_Id
    //         });
    //     });
    //     let infoPostObject = {
    //         id: info.id,
    //         name: info.name,
    //         remarks: info.remarks,
    //         customerPhoneBook: customerPhoneBookToPost,
    //         fK_CustomerType_Id: info.fK_CustomerType_Id
    //     };
    //     console.log(infoPostObject);
    //     this.lookup.updateCustomer(infoPostObject).subscribe(() => {
    //             console.log('success');
    //         },
    //         err => {
    //             console.log('failed');
    //         })
    // };

    closeForm() {
        this.closeFormNow.emit();
    }

    // getPhoneTypes() {
    //     this.lookup.getPhoneTypes().subscribe((phoneTypes) => {
    //             this.phoneTypes = phoneTypes;
    //         },
    //         err => {

    //         })
    // }

    // getCustomerType() {
    //     this.lookup.getCustomerTypes().subscribe((customerTypes) => {
    //             this.customerTypes = customerTypes;
    //         },
    //         err => {

    //         })
    // }

    // postNewCustomer() {
    //     console.log(this.newCustomer);
    //     this.lookup.postCustomer(this.newCustomer).subscribe((res) => {
    //             // this.customerId = res;
    //             this.customerIdValue.emit(res);
    //             console.log('success');
    //         },
    //         err => {
    //             console.log('failed');
    //         });
    //     // console.log(postObj);
    // }

    getGovernorates() {
        return new Promise((resolve, reject) => {
            // (function internalRecursive() {
            // }());
            this.lookup.GetallGovernorates().subscribe((govs) => {
                if (govs[0].name == 'Not Found In Paci') {
                    this.getGovernorates();
                } else {
                    console.log('got governorates');
                    this.governorates = govs;
                    resolve();
                }
            },
                err => {
                    console.log(err);
                });
        });
    }

    getAreas(singleLocation) {
        console.log(singleLocation);
        console.log('in getting areas');
        return new Promise((resolve, reject) => {
            let id = this.selectId(singleLocation, this.governorates);
            console.log(singleLocation);
            console.log(singleLocation.governorate);
            console.log(this.governorates)
            console.log('id' + id);
            // var lookup = this.lookup;
            // (function internalRecursive() {
            //     lookup.GetallAreas(id).subscribe((resAreas) => {
            //         if (resAreas[0].name == 'Not Found In Paci') {
            //             console.log('will retry Areas');
            //             internalRecursive();
            //         } else {
            //         this.areas = resAreas;
            //             resolve();
            //         }
            //     })
            // }());

            this.lookup.GetallAreas(id).subscribe((resAreas) => {
                if (resAreas[0].name == 'Not Found In Paci') {
                    console.log('will retry Areas');
                    // this.getAreas(singleLocation);
                } else {
                    this.areas = resAreas;
                    resolve();
                }
            },
                err => {
                    console.log(err);
                });

        });
    }

    getBlocks(singleLocation) {
        return new Promise((resolve, reject) => {
            let id = this.selectId(singleLocation.area, this.areas);
            console.log(id);
            this.lookup.GetallBlocks(id).subscribe((resBlocks) => {
                if (resBlocks[0].name == 'Not Found In Paci') {
                    console.log('will retry Blocks');
                    this.getBlocks(singleLocation);
                } else {
                    this.blocks = resBlocks;
                    resolve();
                }
            },
                err => {
                    console.log(err);
                });

            // var lookUp = this.lookup;
            // (function internalRecursive() {
            //     lookUp.GetallBlocks(id).subscribe((blocks) => {
            //         if (blocks[0].name == 'Not Found In Paci') {
            //             console.log('will retry blocks');
            //             internalRecursive();
            //         } else {
            //             singleLocation.blocks = blocks;
            //             resolve();
            //         }
            //     },
            //         err => {
            //             console.log('failed');
            //         })
            // })();

        })
    }

    getStreets(singleLocation) {
        // this.toggleLoadingLocation = true;
        return new Promise((resolve, reject) => {
            let govId = this.selectId(singleLocation.governorate, this.governorates);
            let areaId = this.selectId(singleLocation.area, this.areas);

            this.lookup.GetallStreets(govId, areaId, singleLocation.block).subscribe((resStreets) => {
                if (resStreets[0].name == 'Not Found In Paci') {
                    console.log('will retry Streets');
                    this.getStreets(singleLocation);
                    this.toggleLoadingLocation = false;
                } else {
                    this.streets = resStreets;
                    resolve();
                    this.toggleLoadingLocation = false;
                }
            },
                err => {
                    console.log(err);
                    this.toggleLoadingLocation = false;
                });

            //var lookUp = this.lookup;
            // (function internalRecursive() {
            //     lookUp.GetallStreets(govId, areaId, singleLocation.block).subscribe((streets) => {
            //         // this.streets = streets;
            //         if (streets.length > 0) {
            //             if (streets[0].name == 'Not Found In Paci') {
            //                 console.log('will retry streets');
            //                 internalRecursive();
            //             } else {
            //                 console.log('got streets');
            //                 singleLocation.streets = streets;
            //                 resolve();
            //             }
            //         }
            //     },
            //         err => {
            //             console.log('failed');
            //         })
            // })();

        })
    }

    compareByOptionId(idFist, idSecond) {
        return idFist && idSecond && idFist.id == idSecond.id;
    }

    getByPaci(locationIndex) {
        // this.toggleLoadingLocation = true;
        if (this.locationObj[locationIndex].paciNumber >= 8) {
            console.log('getting location by PACI');
            this.lookup.GetLocationByPaci(this.locationObj[locationIndex].paciNumber).subscribe((location) => {
                console.log(location);
                this.toggleLoadingLocation = true;
                if (location.governorate) {
                    this.locationObj[locationIndex].governorate = location.governorate.name;
                }
                if (location.area) {
                    this.areas = [{ name: location.area.name }];
                    this.locationObj[locationIndex].area = location.area.name;
                }
                if (location.block) {
                    this.locationObj[locationIndex].blocks = [{ name: location.block.name }];
                    this.locationObj[locationIndex].block = location.block.name;
                }
                if (location.street) {
                    this.locationObj[locationIndex].streets = [{ name: location.street.name }];
                    this.locationObj[locationIndex].street = location.street.name;
                }
                this.getAreas(this.locationObj[locationIndex].governorate);
                this.getBlocks(this.locationObj[locationIndex]);
                this.getStreets(this.locationObj[locationIndex]);

                this.toggleLoadingLocation = false;
            },
                err => {
                    console.log(err);
                })
        } else {

        }

    }

    selectId(name, list) {
        console.log(name);
        console.log(list);
        var result = list.filter((item) => {
            if (item.name == name) {
                return item.name == name;
            }
        });

        if (result.length) {
            return result[0].id;
        } else {
            return null;
        }
    }

}
