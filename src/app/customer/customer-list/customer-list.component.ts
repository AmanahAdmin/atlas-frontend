import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Angular2Csv } from "angular2-csv";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
// import { Message } from "primeng/components/common/message";
import { TranslateService } from '@ngx-translate/core';
import { Message } from 'primeng/components/common/api';


@Component({
    selector: 'app-customer-list',
    templateUrl: './customer-list.component.html',
    styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
    customers: any[] = [];
    toggleLoading: boolean;
    print: boolean = false;
    cornerMessage: Message[] = [];
    lang = localStorage.getItem('lang');
    page = {
        limit: 10,
        count: 0,
        offset: 0
    };
    searchTxt: string = '';
    firstSearch = true;

    constructor(private lookup: LookupService, private utilities: UtilitiesService, private translate: TranslateService) {
        translate.setDefaultLang(this.lang);
    }

    ngOnInit() {
        this.pageCallback(this.page);
        this.toggleLoading = true;
        // this.getCustomers();
    }

    pageCallback(pageInfo: { count?: number, pageSize?: number, limit?: number, offset?: number }) {
        this.page = {
            count: pageInfo.count,
            limit: pageInfo.limit,
            offset: pageInfo.offset,
        };
        if (this.searchTxt) {
            this.searchCustomers2(this.searchTxt)
        }
        else {
            this.getCustomers();
        }
    }

    getCustomers() {
        this.searchTxt = '';
        this.toggleLoading = true;
        this.lookup.getAllCustomersPaginated(this.page.offset, this.page.limit).subscribe((customers) => {
            // this.customers = customers;
            this.customers = customers["Data"];
            this.page.offset = customers['PageNo'] - 1;
            this.page.limit = customers['PageSize'];
            this.page.count = customers['Count'];
            console.log(this.customers);
            this.toggleLoading = false;
        },
            err => {
                this.toggleLoading = false;
            })
    }
    searchCustomers2(searchText) {
        this.searchTxt = searchText;
        this.toggleLoading = true;
        console.log(searchText);
        this.utilities.currentSearch.searchType = 'customers';
        if (this.firstSearch) {
            this.page = {
                limit: 10,
                count: 0,
                offset: 0
            };
        }
        this.lookup.searchCustomerPagination(this.page.offset, this.page.limit, this.searchTxt).subscribe((result) => {
            // this.customers = result;
            this.firstSearch = false;
            if(result == null){
                this.toggleLoading = false;
                this.cornerMessage.push({
                    severity: "error",
                    summary: this.translate.instant('MSG.fail'),
                    detail: this.translate.instant('MSG.failNoDataDesc')
                });
            }
            else{
            this.customers = result["Data"];
            this.page.offset = result['PageNo'] - 1;
            this.page.limit = result['PageSize'];
            this.page.count = result['Count'];
            console.log(this.customers);
            this.toggleLoading = false;
            if (!result.length) {
                this.cornerMessage.push({
                    severity: "error",
                    summary: this.translate.instant('MSG.fail'),
                    detail: this.translate.instant('MSG.failNoDataDesc')
                });
                setTimeout(() => {
                    this.cornerMessage = [];
                  }, 2000);
            }
            }
        },
            err => {
                this.cornerMessage.push({
                    severity: "error",
                    summary: this.translate.instant('MSG.fail'),
                    detail: this.translate.instant('MSG.failNetworkDesc')
                });
                this.toggleLoading = false;
            })
    }
    printCustomer() {
        this.print = true;
        var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');

        var divToPrint = document.getElementById("customer-table");
        // let newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }
    exportCsv() {
        console.log(this.customers);
        let exportData = [];
        exportData.push({
            'Name': 'Contract Number',
            // 'Civil ID': 'Civil ID',
            'Phone': 'Phone',
            'Address': 'Address'
        });
        this.customers['Data'].map((item) => {
            exportData.push({
                'Contract Number': item.name_en,
                // 'Civil ID': item.civil_id,
                'Phone': item.kinder_person_tel_no,
                'Address': item.address
            })
        });
        return new Angular2Csv(exportData, 'Customers', {
            showLabels: true
        });
    }


}
