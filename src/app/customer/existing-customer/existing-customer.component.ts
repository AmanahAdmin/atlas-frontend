import {Subscription} from 'rxjs/Subscription';
import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {TranslateService} from '@ngx-translate/core';
import { Message } from 'primeng/components/common/api';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditOrderModalComponent } from "../edit-order-modal/edit-order-modal.component";


@Component({
  selector: 'app-existing-customer',
  templateUrl: './existing-customer.component.html',
  styleUrls: ['./existing-customer.component.css']
})

export class ExistingCustomerComponent implements OnInit, OnDestroy {
  // public selectedTab = 'tab1';
  public existCust = false;
  private callId: number;
  private customerId: number;
  private sub: any;
  existedOrders: any[];
  existedCalls: any[];
  existedContracts: any[];
  callData: any;
  existedCustomerData: any = {};
  workOrders;
  activeTab: string;
  orderByCustomerSubscription: Subscription;
  callByCustomerSubscription: Subscription;
  contractsByCustomerSubscription: Subscription;
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');
  allOrders: any = [];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  page: any = {
    limit: 10,
    count: 0,
    offset: 0
  };
  firstSearch = true;
  searchText: string = '';
  modalRef: any;
  buttonsList: any[];
  activeRow: any;
  deleteOrderSubscription: Subscription;

  
  constructor(private router: Router, private route: ActivatedRoute,private lookup: LookupService,private modalService: NgbModal,
              private customerService: CustomerCrudService, private alertService: AlertServiceService, private utilities: UtilitiesService, private lookupService: LookupService,private translate: TranslateService) {
                translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.callData = {};
    // //console.log('existed customer component');
    this.existedOrders = [];
    this.existedCalls = [];
    this.sub = this.route.params.subscribe(params => {
      if (params['customerId']) {
        this.customerId = +params['customerId'];
        console.log('find customer')
        this.getCustomerDetails();
      } 
      // else {
      //   console.log('find call')
      //   this.callId = +params['CurrentCustomer'];
      //   this.GetCallDetails();
      // }
    });
    this.activeTab = 'info';
    this.toggleLoading = true;
    this.translate.get('hello.world').subscribe((translated: string) => {
      // this.pageCallback(this.page);
      this.getOrdersByCustID();
      this.buttonsList = [
        {
          label: this.translate.instant('SHARED.REMOVE'), icon: "fa fa-times", command: () => {
            this.remove(this.activeRow.id)
          }
        }];
    });
  }

  getOrdersByCustID(){
    this.toggleLoading = true;
    this.orderByCustomerSubscription = this.lookupService.getOrderByCustomerId(this.customerId).subscribe((orders) => {
      this.existedOrders = orders;
      console.log(JSON.stringify(this.existedOrders))
      this.toggleLoading = false;
    },
    err => {
      //console.log(err);
    })
  }
  
  pageCallback(pageInfo: { count?: number, limit?: number, offset?: number }) {
    this.page = {
      count: pageInfo.count,
      limit: pageInfo.limit,
      offset: pageInfo.offset,
    };
    if (this.searchText) {
      this.searchByValue(this.searchText)
    }
    else {
      this.getAllOrders();
    }
  }

  getAllOrders() {
    this.toggleLoading = true;
    this.lookup.getAllOrdersPaginated(this.page.offset, this.page.limit).subscribe((allOrders) => {
      this.toggleLoading = false;
      this.allOrders = allOrders["data"];
      console.log(allOrders)

      this.page.offset = allOrders['pageNo'] - 1;
      this.page.limit = allOrders['pageSize'];
      this.page.count = allOrders['count'];
      this.bigMessage = [];
      this.bigMessage.push({
        severity: "info",
        summary: this.translate.instant('MSG.dropdownArow'),
        detail: this.translate.instant('MSG.dropInfoOrdrDesc')
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }
  searchByValue(searchText) {
    this.searchText = searchText;
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'orders';
    if (this.firstSearch) {
      this.page = {
        limit: 10,
        count: 0,
        offset: 0
      };
    }
    this.lookup.getSearchOrdersPaginated(this.page.offset, this.page.limit, this.searchText).subscribe((searchResult) => {
      this.allOrders = searchResult;
      this.toggleLoading = false;
      this.firstSearch = false;
      this.allOrders = searchResult["data"];
      this.page.offset = searchResult['pageNo'] - 1;
      this.page.limit = searchResult['pageSize'];
      this.page.count = searchResult['count'];
      this.toggleLoading = false;
      if (!this.allOrders.length) {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNoDataDesc')
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      }
    },
      err => {
        this.toggleLoading = false;
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.orderByCustomerSubscription && this.orderByCustomerSubscription.unsubscribe();
    this.callByCustomerSubscription && this.callByCustomerSubscription.unsubscribe();
  }

  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    console.log(id)
    this.router.navigate(['search/contract/', id])
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    if (tab == 'orders') {
      // this.toggleLoading = true;
      this.orderByCustomerSubscription = this.lookupService.getOrderByCustomerId(this.customerId).subscribe((orders) => {
          this.existedOrders = orders;
          console.log(JSON.stringify(this.existedOrders))
          this.toggleLoading = false;
        },
        err => {
          //console.log(err);
        })
    } 
    else if (tab == 'contracts') {
      // this.toggleLoading = true;
      this.contractsByCustomerSubscription = this.lookupService.getContractByCustomerId(this.customerId).subscribe((contracts) => {
          this.existedContracts = contracts;
          console.log(JSON.stringify(this.existedContracts));
          this.toggleLoading = false;
        },
        err => {
          //console.log(err);
        })
    }
    else if (tab == 'calls') {
      this.callByCustomerSubscription = this.lookupService.getCallByCustomerId(this.callData.callerNumber).subscribe((calls) => {
          this.existedCalls = calls;
        },
        err => {
          //console.log(err);
        })
    }
    else{
      this.getCustomerDetails();
    }
  }

  getCustomerDetails() {
    this.lookupService.getCustomerById(this.customerId).subscribe((existedCustomerData) => {
        this.existedCustomerData = existedCustomerData;
        console.log(this.existedCustomerData);
      },
      err => {
        console.log(err);
      })
  }

  GetCallDetails() {
    //console.log('will get customer details')
    this.toggleLoading = true;
    this.customerService.GetCallDetail(this.callId).subscribe(callData => {
        this.callData = callData;
        console.log(this.callData);
        if (this.callData.fK_Customer_Id) {
          this.customerId = this.callData.fK_Customer_Id;
          this.getCustomerDetails();
        } else {
          this.utilities.updateCurrentExistedCustomer(callData);
        }
        this.toggleLoading = false;
      },
      err => {
        this.alertService.error('we have Server Error !')
      });
  }

  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "success",
          summary: this.translate.instant('MSG.ordrSave'),
          detail: this.translate.instant('MSG.ordrDescSav')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
      .catch(() => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "info",
          summary: this.translate.instant('MSG.calncel'),
          detail: this.translate.instant('MSG.cnclEdtDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }
  openModal(data) {
    this.modalRef = this.modalService.open(EditOrderModalComponent);
    this.modalRef.componentInstance.data = data;
  }

  remove(id) {
    //console.log(id);
    this.deleteOrderSubscription = this.lookup.deleteOrder(id).subscribe(() => {
      window.scroll(0,0);
      this.cornerMessage.push({
        severity: "success",
        summary: this.translate.instant('MSG.removSuces'),
        detail: this.translate.instant('MSG.ordrRemovSuces')
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
      this.allOrders = this.allOrders.filter((order) => {
        return order.id != id;
      })
    },
      err => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

}
