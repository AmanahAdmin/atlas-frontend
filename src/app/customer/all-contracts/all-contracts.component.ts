import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
// import {MenuItem} from
// import {MessageService} from 'primeng/components/common/messageservice';
import { Message } from "primeng/components/common/message";
import { Router } from "@angular/router";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { GenerateContractModalComponent } from "../generate-contract-modal/generate-contract-modal.component";
import { SearchComponent } from "../../shared-module/search/search.component";
import { TranslateService } from '@ngx-translate/core';
import { Angular2Csv } from "angular2-csv";


@Component({
  selector: 'app-all-contracts',
  templateUrl: './all-contracts.component.html',
  styleUrls: ['./all-contracts.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllContractsComponent implements OnInit, OnDestroy {
  contracts: any = [];
  allContractSubscription: Subscription;
  buttonsList: any[];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  lang = localStorage.getItem('lang');
  page = {
    limit: 10,
    count: 0,
    offset: 0
  };
  firstSearch = true;
  searchText: string = '';
  activeRow: any;
  deleteContractSubscription: Subscription;

  constructor(private lookup: LookupService, private utilities: UtilitiesService, private modalService: NgbModal,
    private translate: TranslateService, private router: Router) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.pageCallback(this.page);
  }

  pageCallback(pageInfo: { count?: number, limit?: number, offset?: number }) {
    this.page = {
      count: pageInfo.count,
      limit: pageInfo.limit,
      offset: pageInfo.offset,
    };
    if (this.searchText) {
      this.searchContract(this.searchText)
    }
    else {
      this.getAllContracts();
    }
  }

  getAllContracts() {
    this.searchText = '';
    this.toggleLoading = true;
    this.lookup.getAllContractsPaginated(this.page.offset, this.page.limit).subscribe((allContracts) => {
      // this.contracts = allContracts;
      if(allContracts == null){
        this.toggleLoading = false;
        this.cornerMessage.push({
            severity: "error",
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNoDataDesc')
        });
    }
      this.contracts = allContracts["Data"];
      this.page.offset = allContracts['PageNo'] - 1;
      this.page.limit = allContracts['PageSize'];
      this.page.count = allContracts['Count'];
      console.log(this.contracts);
      this.toggleLoading = false;
      this.bigMessage = [];
      this.bigMessage.push({
        severity: 'info',
        summary: this.translate.instant('MSG.dropdownArow'),
        detail: this.translate.instant('MSG.dropInfoContDesc')
      });
      //console.log(this.allContracts);
      // this.cd.markForCheck();
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage = [];
        this.cornerMessage.push({
          severity: 'error',
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkContDesc')
        });


      });
  }

  searchContract(searchText) {
    this.searchText = searchText;
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'contract';
    if (this.firstSearch) {
      this.page = {
        limit: 10,
        count: 0,
        offset: 0
      };
    }
    this.lookup.searchContractPagination(this.page.offset, this.page.limit, this.searchText).subscribe((allContracts) => {
      console.log(this.contracts);
      if(allContracts == null){
        this.toggleLoading = false;
        window.scroll(0,0);
        this.cornerMessage.push({
            severity: "error",
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNoDataDesc')
        });
    }
    else{
      this.contracts = allContracts["Data"];
      this.page.offset = allContracts['PageNo'] - 1;
      this.page.limit = allContracts['PageSize'];
      this.page.count = allContracts['Count'];
      this.toggleLoading = false;
      if (!this.contracts.length) {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNoDataDesc')
        });

        setTimeout(() => {
          //console.log('will route');
          this.cornerMessage = [];
        }, 2000);
      }
    }
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        });
        this.toggleLoading = false;
      })
  }

  ngOnDestroy() {
    this.allContractSubscription && this.allContractSubscription.unsubscribe();
  }

  openAddContractModal() {
    console.log('open modal');
    let contractModal = this.modalService.open(GenerateContractModalComponent);
    contractModal.result.then(() => {
      this.getAllContracts();
    })
      .catch(() => {

      })
  }

  printContracts(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=500px,width=500px');

    var divToPrint = document.getElementById("contract-table");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  exportCsv() {
    console.log(this.contracts);
    let exportData = [];
    exportData.push({
      'Contract Number': 'Contract Number',
      'Contract Type': 'Contract Type',
      // 'Civil ID': 'Civil ID',
      // 'Tenants Passport': 'Tenants Passport',
      'Owner Name': 'Owner Name',
      'Real Name': 'Real Name',
      'Start Date': 'Start Date',
      'End Date': 'End Date',
      // 'Price': 'Price'
    });
    this.contracts.map((item) => {
      exportData.push({
        'Contract Number': item.cont_no,
        'Contract Type': item.contract_type,
        // 'Civil ID': item.civil_id,
        // 'Tenants Passport': item.tenants_passport,
        'Real Name': item.real_name,
        'Owner Name': item.owner_name,
        'Start date': item.from_dt,
        'End Date': item.to_date,
        // 'Price': item.amount
      })
    });
    return new Angular2Csv(exportData, 'Contracts Report', {
      showLabels: true
    });
  }

  removeContract(ContractId) {
    //console.log(ContractId);
    this.deleteContractSubscription = this.lookup.deleteContract(ContractId).subscribe(() => {
      window.scroll(0,0);
      this.cornerMessage.push({
        severity: 'success',
        summary: this.translate.instant('MSG.removSuces'),
        detail: this.translate.instant('MSG.contRemovSuces')
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
      this.contracts = this.contracts.filter((contract) => {
        return contract.id != ContractId;
      });
    },
      err => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: 'error',
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkContDesc')
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    console.log(this.activeRow);
  }

  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    console.log(id)
    this.router.navigate(['search/contract/', id])
  }

}
