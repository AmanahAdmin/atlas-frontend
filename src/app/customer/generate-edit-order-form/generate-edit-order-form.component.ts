import {Component, OnInit, Output, EventEmitter, Input, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {MessageService} from "primeng/components/common/messageservice";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-generate-edit-order-form',
  templateUrl: './generate-edit-order-form.component.html',
  styleUrls: ['./generate-edit-order-form.component.css']
})

export class GenerateEditOrderFormComponent implements OnInit, OnDestroy {
  @Output() order = new EventEmitter;
  @Output() close = new EventEmitter;
  @Input() existedOrder: any;
  @Input() customerId: any;
  @Input() disabled: boolean;
  newOrder: any;
  orderTypeSubscription: Subscription;
  orderStatusSubscription: Subscription;
  orderPrioritySubscription: Subscription;
  orderProblemSubscription: Subscription;
  customerLocationsSubscription: Subscription;
  customerInfoSubscription: Subscription;
  todayDate: Date;
  orderTypes: any[];
  statuses: any[];
  orderPriority: any[];
  customerLocations: any[];
  problems: any[];
  isNewOrder: boolean;
  existedCustomerData:any = {};

  constructor(private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    //console.log(this.existedOrder);
    if (this.existedOrder) {
      this.isNewOrder = false;
      this.existedOrder.startDate = new Date(this.existedOrder.startDate);
      this.existedOrder.endDate = new Date(this.existedOrder.endDate);
      this.newOrder = this.existedOrder;
      this.customerId = this.existedOrder.fK_Customer_Id;
      console.log(this.newOrder)
    } else {
      this.isNewOrder = true;
      this.newOrder = {};
      this.newOrder.fK_OrderStatus_Id = 0;
    }

    this.orderTypeSubscription = this.lookup.getOrderType().subscribe((orderTypes) => {
        this.orderTypes = orderTypes;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderPrioritySubscription = this.lookup.getOrderPriority().subscribe((orderPriority) => {
        this.orderPriority = orderPriority;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderProblemSubscription = this.lookup.getAllProblems().subscribe((problems) => {
        this.problems = problems;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });
    console.log(this.customerId);
    this.customerLocationsSubscription = this.lookup.getLocationByCustomerId(this.customerId).subscribe((locations) => {
        this.customerLocations = locations;
        console.log(this.customerLocations);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderStatusSubscription = this.lookup.getOrderStatus().subscribe((status) => {
        this.statuses = status;
        //console.log(this.customerLocations);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });
    this.todayDate = new Date();
    console.log(this.todayDate)
    this.customerInfoSubscription =  this.lookup.getCustomerById(this.customerId).subscribe((existedCustomerData) => {
      this.existedCustomerData = existedCustomerData;
      console.log('existedCustomerData' + JSON.stringify(this.existedCustomerData));
    },
    err => {
      console.log(err);
    })
  }

  submitOrder(order) {
    console.log(order);
    this.order.emit(order);
  }

  closeModal() {
    this.close.emit();
  }

  ngOnDestroy() {
    this.orderTypeSubscription.unsubscribe();
    this.orderPrioritySubscription.unsubscribe();
    this.customerLocationsSubscription.unsubscribe();
  }

  stringDate(date) {
    let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(),
        date.getMinutes(),
        date.getSeconds()));
    return d.toISOString();
}


}
