import {Component, OnInit, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {Subscription} from "rxjs";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Message} from "primeng/components/common/message";
import {Router} from "@angular/router";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {Angular2Csv} from "angular2-csv";

// import {Http} from "@angular/http";

@Component({
  selector: 'app-contract-table',
  templateUrl: './contract-table.component.html',
  styleUrls: ['./contract-table.component.css']
})

export class ContractTableComponent implements OnInit, OnChanges {
  @Input() contracts
  @Output() print = new EventEmitter();
  activeRow: any;
  deleteContractSubscription: Subscription;
  buttonsList: any[];
  cornerMessage: Message[] = [];

  constructor(private lookup: LookupService, private router: Router, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    // this.contracts = [];
    console.log(this.contracts)

    this.buttonsList = [
      {
        label: 'Remove', icon: 'fa fa-times', command: () => {
          this.removeContract(this.activeRow.id);
        }
      }
    ];
  }

  ngOnChanges() {
    if (this.contracts && this.contracts.length > 0) {
      this.contracts.map((contract) => {
        contract.startDate = this.utilities.convertDatetoNormal(contract.startDate);
        contract.endDate = this.utilities.convertDatetoNormal(contract.endDate);
      });
    }
  }

  exportCsv() {
    console.log(this.contracts);
    let exportData = [];
    exportData.push({
      'Contract Number': 'Contract Number',
      // 'Tenants Name': 'Tenants Name',
      // 'Civil ID': 'Civil ID',
      // 'Tenants Passport': 'Tenants Passport',
      'Owner Name': 'Owner Name',
      'Real Name': 'Real Name',
      'Start Date': 'Start Date',
      'End Date': 'End Date',
      // 'Price': 'Price'
    });
    this.contracts.map((item) => {
      exportData.push({
        'Contract Number': item.cont_no,
        // 'Tenants Name': item.tenants_name,
        // 'Civil ID': item.civil_id,
        // 'Tenants Passport': item.tenants_passport,
        'Real Name': item.real_name,
        'Owner Name': item.owner_name,
        'Start date': item.from_dt,
        'End Date': item.to_date,
        // 'Price': item.amount
      })
    });
    return new Angular2Csv(exportData, 'Contracts Report', {
      showLabels: true
    });
  }

  emitPrint(): void {
    this.print.emit();
  }

  routeToEstimation(id) {
    this.router.navigate(['/search/editEstimation/', id]);
  }

  removeContract(ContractId) {
    //console.log(ContractId);
    this.deleteContractSubscription = this.lookup.deleteContract(ContractId).subscribe(() => {
        this.cornerMessage.push({
          severity: 'success',
          summary: 'Successfully!',
          detail: 'Contract removed Successfully!'
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
        this.contracts = this.contracts.filter((contract) => {
          return contract.id != ContractId;
        });
      },
      err => {
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to remove contract due to server error!'
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    console.log(this.activeRow);
  }

  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    console.log(id)
    this.router.navigate(['search/contract/', id])
  }

}
