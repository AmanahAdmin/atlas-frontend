import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllEstimationsTableComponent } from './all-estimations-table.component';

describe('AllEstimationsTableComponent', () => {
  let component: AllEstimationsTableComponent;
  let fixture: ComponentFixture<AllEstimationsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllEstimationsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEstimationsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
