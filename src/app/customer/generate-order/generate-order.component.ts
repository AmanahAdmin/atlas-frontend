import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
import {MessageService} from "primeng/components/common/messageservice";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {Message} from "primeng/components/common/message";
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-generate-order',
  templateUrl: './generate-order.component.html',
  styleUrls: ['./generate-order.component.css']
})

export class GenerateOrderComponent implements OnInit, OnDestroy {
  contractDetails: any;
  postNewOrderSubscription: Subscription;
  /*this flag to check if the date already converted while post the order*/
  dateConvertedFlag: boolean;
  customerId: any;
  toggleLoading: boolean;
  toggleOrderSectionLoading: boolean;
  todayDate: Date;
  msg: Message[];
  lang = localStorage.getItem('lang');
  existedCustomerData:any = {};


  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService, private messageService: MessageService, 
    private router: Router, private utilities: UtilitiesService, private translate: TranslateService) {
      translate.setDefaultLang(this.lang);
  
  }

  ngOnInit() {
    this.getCustomerDetails();
    
    this.toggleLoading = true;
    this.dateConvertedFlag = false;
    this.utilities.routingFromAndHaveSearch = true;
    this.activatedRoute.params.forEach((params: Params) => {
      this.lookup.getContractById(+params['id']).subscribe((contractDetails) => {
          //console.log(contractDetails);
          this.contractDetails = contractDetails;
          this.contractDetails.from_dt = this.utilities.convertDatetoNormal(this.contractDetails.from_dt);
          this.contractDetails.to_date = this.utilities.convertDatetoNormal(this.contractDetails.to_date);
          this.customerId = this.contractDetails.customer_id;
          this.contractDetails.tenants_name = this.contractDetails.tenants_name;
          this.contractDetails.civil_id = this.contractDetails.civil_id;
          this.toggleLoading = false;
          console.log(this.contractDetails)
        },
        err => {
          this.toggleLoading = false;
        });
    });
  }

  
getCustomerDetails() {
  this.lookup.getCustomerById(this.customerId).subscribe((existedCustomerData) => {
      this.existedCustomerData = existedCustomerData;
      console.log('existedCustomerData' + this.existedCustomerData);
    },
    err => {
      console.log(err);
    })
}

  ngOnDestroy() {
    this.postNewOrderSubscription && this.postNewOrderSubscription.unsubscribe();
  }

  generateOrder(newOrder) {
    this.toggleOrderSectionLoading = true;
    console.log('newOrder');
    
    /*form the json object to be send*/
    newOrder.StartDate = this.dateConvertedFlag ? newOrder.startDate : newOrder.startDate.toISOString();
    newOrder.EndDate = this.dateConvertedFlag ? newOrder.endDate : newOrder.endDate.toISOString();
    newOrder.fK_Customer_Id = this.contractDetails.customer_id;
    newOrder.fK_Contract_Id = this.contractDetails.contract_id;
    console.log('fK_Contract_Id' + this.contractDetails.contract_id)
    // newOrder.fK_Location_Id = this.contractDetails.fk_location_id;
    // if (this.contractDetails.contractQuotations[0]) {
    //   newOrder.quotationRefNo = this.contractDetails.contractQuotations[0].quotationRefNumber;
    // }
    newOrder.price = this.contractDetails.price;

    // //console.log(this.contractDetails.contractQuotations[0]);
    console.log(newOrder);
    this.dateConvertedFlag = true;
    /*post the json object*/
    this.postNewOrderSubscription = this.lookup.postNewOrder(newOrder).subscribe(() => {
      window.scroll(0,0);
        this.messageService.add({
          severity: 'success',
          summary: 'Order Generated!',
          detail: 'Order Generated successfully!'
        });
        this.toggleOrderSectionLoading = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/order'])
        }, 2000);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        window.scroll(0,0);
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to generate new Order due to network error.'
        })
        this.toggleOrderSectionLoading = false;
      })
  }

}
