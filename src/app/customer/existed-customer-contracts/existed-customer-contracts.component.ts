import {Component, OnInit, Input} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from '@ngx-translate/core';
import {Router} from "@angular/router";
import { Message } from "primeng/components/common/message";
import {Angular2Csv} from "angular2-csv";



@Component({
  selector: 'app-existed-customer-contracts',
  templateUrl: './existed-customer-contracts.component.html',
  styleUrls: ['./existed-customer-contracts.component.css']
})

export class ExistedCustomerContractsComponent implements OnInit {

  contracts: any = [];
  lang = localStorage.getItem('lang');
  page = {
    limit: 10,
    count: 0,
    offset: 0
  };
  firstSearch = true;
  searchText: string = '';
  activeRow: any;
  deleteContractSubscription: Subscription;  
  buttonsList: any[];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;


  constructor(private lookup: LookupService, private utilities: UtilitiesService, private modalService: NgbModal,
    private translate: TranslateService, private router: Router) {
    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    this.pageCallback(this.page);
    this.buttonsList = [
      {
        label: 'Remove', icon: 'fa fa-times', command: () => {
          this.removeContract(this.activeRow.id);
        }
      }
    ];
  }

  pageCallback(pageInfo: { count?: number, limit?: number, offset?: number }) {
    this.page = {
      count: pageInfo.count,
      limit: pageInfo.limit,
      offset: pageInfo.offset,
    };
    if (this.searchText) {
      this.searchContract(this.searchText)
    }
    else {
      this.getAllContracts();
    }
  }

  
  getAllContracts() {
    this.searchText = '';
    this.toggleLoading = true;
    this.lookup.getAllContractsPaginated(this.page.offset, this.page.limit).subscribe((allContracts) => {
      // this.contracts = allContracts;
      this.contracts = allContracts["Data"];
      this.page.offset = allContracts['PageNo'] - 1;
      this.page.limit = allContracts['PageSize'];
      this.page.count = allContracts['Count'];
      console.log(this.contracts);
      this.toggleLoading = false;
      this.bigMessage = [];
      this.bigMessage.push({
        severity: 'info',
        summary: this.translate.instant('MSG.dropdownArow'),
        detail: this.translate.instant('MSG.dropInfoContDesc')
      });
      //console.log(this.allContracts);
      // this.cd.markForCheck();
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage = [];
        this.cornerMessage.push({
          severity: 'error',
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkContDesc')
        });


      });
  }

  searchContract(searchText) {
    this.searchText = searchText;
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'contract';
    debugger;
    if (this.firstSearch) {
      this.page = {
        limit: 10,
        count: 0,
        offset: 0
      };
    }
    this.lookup.searchContractPagination(this.page.offset, this.page.limit, this.searchText).subscribe((allContracts) => {
      console.log(this.contracts);
      this.firstSearch = false;
      this.contracts = allContracts["Data"];
      this.page.offset = allContracts['PageNo'] - 1;
      this.page.limit = allContracts['PageSize'];
      this.page.count = allContracts['Count'];
      this.toggleLoading = false;
      if (!this.contracts.length) {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNoDataDesc')
        });

        setTimeout(() => {
          //console.log('will route');
          this.cornerMessage = [];
        }, 2000);
      }
    },
      err => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        });
        this.toggleLoading = false;
      })
  }

  ngOnChanges() {
    if (this.contracts && this.contracts.length > 0) {
      this.contracts.map((contract) => {
        contract.startDate = this.utilities.convertDatetoNormal(contract.startDate);
        contract.endDate = this.utilities.convertDatetoNormal(contract.endDate);
      });
    }
  }

  routeToEstimation(id) {
    this.router.navigate(['/search/editEstimation/', id]);
  }

  removeContract(ContractId) {
    //console.log(ContractId);
    this.deleteContractSubscription = this.lookup.deleteContract(ContractId).subscribe(() => {
      window.scroll(0,0);
        this.cornerMessage.push({
          severity: 'success',
          summary: 'Successfully!',
          detail: 'Contract removed Successfully!'
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
        this.contracts = this.contracts.filter((contract) => {
          return contract.id != ContractId;
        });
      },
      err => {
        window.scroll(0,0);
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to remove contract due to server error!'
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    console.log(this.activeRow);
  }

  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    console.log(id)
    this.router.navigate(['search/contract/', id])
  }

}
