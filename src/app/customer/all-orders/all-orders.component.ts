import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { Message } from 'primeng/components/common/api';
// import {EditOrderModalComponent} from "../edit-order-modal/edit-order-modal.component"
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { EditOrderModalComponent } from "../edit-order-modal/edit-order-modal.component"
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllOrdersComponent implements OnInit, OnDestroy {
  allOrdersSubscription: Subscription;
  searchSubscription: Subscription;
  allOrders: any = [];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');

  page: any = {
    limit: 10,
    count: 0,
    offset: 0
  };
  firstSearch = true;
  searchText: string = '';
  modalRef: any;
  buttonsList: any[];
  activeRow: any;
  deleteOrderSubscription: Subscription;
  
  constructor(private lookup: LookupService, private utilities: UtilitiesService, private translate: TranslateService,
    private modalService: NgbModal) {
    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    this.toggleLoading = true;
    this.translate.get('hello.world').subscribe((translated: string) => {
      this.pageCallback(this.page);
      this.buttonsList = [
        {
          label: this.translate.instant('SHARED.REMOVE'), icon: "fa fa-times", command: () => {
            this.remove(this.activeRow.id)
          }
        }];
    });
  }

  pageCallback(pageInfo: { count?: number, limit?: number, offset?: number }) {
    this.page = {
      count: pageInfo.count,
      limit: pageInfo.limit,
      offset: pageInfo.offset,
    };
    if (this.searchText) {
      this.searchByValue(this.searchText)
    }
    else {
      this.getAllOrders();
    }
  }

  getAllOrders() {
    this.toggleLoading = true;
    this.lookup.getAllOrdersPaginated(this.page.offset, this.page.limit).subscribe((allOrders) => {
      this.toggleLoading = false;
      this.allOrders = allOrders["data"];
      console.log(allOrders)

      this.page.offset = allOrders['pageNo'] - 1;
      this.page.limit = allOrders['pageSize'];
      this.page.count = allOrders['count'];
      this.bigMessage = [];
      this.bigMessage.push({
        severity: "info",
        summary: this.translate.instant('MSG.dropdownArow'),
        detail: this.translate.instant('MSG.dropInfoOrdrDesc')
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
    },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  ngOnDestroy() {
    //  this.allOrdersSubscription.unsubscribe();
    // this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
    //this.searchSubscription && this.searchSubscription.unsubscribe();
  }

  searchByValue(searchText) {
    this.searchText = searchText;
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'orders';
    if (this.firstSearch) {
      this.page = {
        limit: 10,
        count: 0,
        offset: 0
      };
    }
    this.lookup.getSearchOrdersPaginated(this.page.offset, this.page.limit, this.searchText).subscribe((searchResult) => {
      this.allOrders = searchResult;
      this.toggleLoading = false;
      this.firstSearch = false;
      if(searchResult == null){
        this.toggleLoading = false;
        this.cornerMessage.push({
            severity: "error",
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNoDataDesc')
        });
    }
    else{
      this.allOrders = searchResult["data"];
      this.page.offset = searchResult['pageNo'] - 1;
      this.page.limit = searchResult['pageSize'];
      this.page.count = searchResult['count'];
      this.toggleLoading = false;
      if (!this.allOrders.length) {
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNoDataDesc')
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      }
    }
    },
      err => {
        this.toggleLoading = false;
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }
  //// ==== /////

  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        this.cornerMessage.push({
          severity: "success",
          summary: this.translate.instant('MSG.ordrSave'),
          detail: this.translate.instant('MSG.ordrDescSav')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
      .catch(() => {
        this.cornerMessage.push({
          severity: "info",
          summary: this.translate.instant('MSG.calncel'),
          detail: this.translate.instant('MSG.cnclEdtDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }
  openModal(data) {
    this.modalRef = this.modalService.open(EditOrderModalComponent);
    this.modalRef.componentInstance.data = data;
  }

  remove(id) {
    //console.log(id);
    this.deleteOrderSubscription = this.lookup.deleteOrder(id).subscribe(() => {
      this.cornerMessage.push({
        severity: "success",
        summary: this.translate.instant('MSG.removSuces'),
        detail: this.translate.instant('MSG.ordrRemovSuces')
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
      this.allOrders = this.allOrders.filter((order) => {
        return order.id != id;
      })
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

}
