import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { EditOrderModalComponent } from "../edit-order-modal/edit-order-modal.component"
import { Subscription } from "rxjs";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message } from 'primeng/components/common/api';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})

export class OrderTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() orders: any[];
  deleteOrderSubscription: Subscription;
  cornerMessage: Message[] = [];
  modalRef: any;
  buttonsList: any[];
  activeRow: any;
  lang = localStorage.getItem('lang');


  constructor(private lookup: LookupService, private modalService: NgbModal, private utilities: UtilitiesService,
    private translate: TranslateService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    // this.orders = [];
    this.translate.get('hello.world').subscribe((translated: string) => {
      this.buttonsList = [
        {
          label: this.translate.instant('SHARED.REMOVE'), icon: "fa fa-times", command: () => {
            this.remove(this.activeRow.id)
          }
        }
      ];
    })

  }

  ngOnChanges() {
    //console.log(this.orders);
    this.orders = this.orders || [];
    this.orders.map((order) => {
      order.startDateView = this.utilities.convertDatetoNormal(order.startDate);

      order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
    });
  }

  ngOnDestroy() {
    this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
  }

  setActiveRow(row) {
    this.activeRow = row;
  }

  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        this.cornerMessage.push({
          severity: "success",
          summary: "Order Saved!",
          detail: "Order edits applied & Saved successfully."
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
      .catch(() => {
        this.cornerMessage.push({
          severity: "info",
          summary: "Cancelled!",
          detail: "Order Edits cancelled without saving."
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  openModal(data) {
    this.modalRef = this.modalService.open(EditOrderModalComponent);
    this.modalRef.componentInstance.data = data;
  }

  remove(id) {
    //console.log(id);
    this.deleteOrderSubscription = this.lookup.deleteOrder(id).subscribe(() => {
      this.cornerMessage.push({
        severity: "success",
        summary: "Removed successfully",
        detail: "Order removed successfully."
      });
      setTimeout(() => {
        this.cornerMessage = [];
      }, 2000);
      this.orders = this.orders.filter((order) => {
        return order.id != id;
      })
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data dut to network error, please try again later."
        })
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000);
      })
  }

  // private _convertDatetoNormal(date) {
  //   date = new Date(`${date}`);
  //   let year = date.getFullYear();
  //   let month = date.getMonth() + 1;
  //   let day = date.getDate();
  //
  //   if (day < 10) {
  //     day = '0' + day;
  //   }
  //   if (month < 10) {
  //     month = '0' + month;
  //   }
  //   let formatedDate = `${day}/${month}/${year}`;
  //   //console.log(formatedDate);
  //   return formatedDate;
  // }


}
