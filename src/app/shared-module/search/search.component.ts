import {Component, OnInit, ViewEncapsulation, Output, Input, EventEmitter} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class SearchComponent implements OnInit {
  @Input() placholder: string;
  @Input() hideFromTo: boolean;
  @Output() searchValue = new EventEmitter;
  @Output() searchCanceled = new EventEmitter;
  @Input() searchText: string;
  @Input() firstTime: boolean;
  fromDate: any;
  toDate: any;
  errorFlag: boolean;
  submitted: boolean = false;
  
  constructor(private utilities: UtilitiesService) {
  }
  
  ngOnInit() {
    console.log(this.fromDate);
    console.log(this.toDate);
    console.log('init search component');
    this.firstTime = false;
    if (this.hideFromTo === undefined) {
      this.hideFromTo = true;
    } else {
      this.hideFromTo = false;
    }
  }
  
  clearSearch() {
    this.errorFlag = false;
    this.searchText = '';
    this.fromDate = '';
    this.toDate = '';
    this.utilities.currentSearch = this.searchText;
    this.searchCanceled.emit();
  }
  
  toggleErrorFlag() {
    this.errorFlag = true;
  }
  
  outPutSearchValue(formValues) {
    this.submitted = true;
    console.log(formValues);
    console.log(this.toDate);
    if (!this.hideFromTo) {
      console.log(formValues.value);
      let fromDateToPost = formValues.value.fromDate && new Date(formValues.value.fromDate);
      let toDateToPost = formValues.value.toDate && new Date(formValues.value.toDate);
      // console.log(fromDateToPost.toISOString());
      let searchObject = {};
      if (formValues.value.searchText || fromDateToPost || toDateToPost) {
        formValues.value.searchText && (searchObject['searchText'] = formValues.value.searchText);
        fromDateToPost && (searchObject['fromDate'] = fromDateToPost.toISOString());
        toDateToPost && (searchObject['toDate'] = toDateToPost.toISOString());
      }
      console.log(searchObject);
      this.utilities.currentSearch = searchObject;
      this.searchValue.emit(searchObject);
    } else {
      this.utilities.currentSearch = {searchText: formValues.value.searchText};
      this.searchValue.emit(formValues.value.searchText);
    }
  }
}
