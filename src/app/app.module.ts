import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {SharedModuleModule} from './shared-module/shared-module.module';
// import {SignalRService} from './shared-module/signal-r.service';
import {RoutingModule, RoutingComponents} from './router.module';
import {FormsModule} from '@angular/forms';
import {HttpModule, BaseRequestOptions} from '@angular/http';
// import {SpinnerModule} from 'angular2-spinner/src/';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {MessageService} from 'primeng/components/common/messageservice';
import {UtilitiesService} from "./api-module/services/utilities/utilities.service";
import { AngularFontAwesomeModule } from 'angular-font-awesome';

let token = '';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationServicesService} from "./api-module/services/authentication/authentication-services.service";

import {LocationStrategy, HashLocationStrategy} from '@angular/common'

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
   declarations: [
    AppComponent,
    RoutingComponents
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ProgressSpinnerModule,
    SharedModuleModule.forRoot(),
    AngularFontAwesomeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    BaseRequestOptions, UtilitiesService, MessageService, AuthenticationServicesService],
  bootstrap: [AppComponent],


})
export class AppModule { }
