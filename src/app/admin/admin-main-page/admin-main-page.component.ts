import {Router} from '@angular/router';
import {Component, OnInit, Input} from '@angular/core';
import {Message} from "primeng/components/common/message";
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-admin-main-page',
  templateUrl: './admin-main-page.component.html',
  styleUrls: ['./admin-main-page.component.css', '../../customer/existing-customer/existing-customer.component.css']
})

export class AdminMainPageComponent implements OnInit {
  getDataForType: string;
  items: string[];
  activeTab: string;
  getUserDataFlag: boolean;
  msg: Message[];
  lang = localStorage.getItem('lang');

  constructor(private router: Router,private translate: TranslateService) {
    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    
    this.translate.get('hello.world').subscribe((translated: string) => {
    this.items = [
      // this.translate.instant('ADMN.CUSTTYPE'), this.translate.instant('ADMN.CALTYPE'), 
      // this.translate.instant('ADMN.CALPRORTY'), this.translate.instant('ADMN.PHONTYP'), this.translate.instant('ADMN.ACTNSTUTS'), 
      // this.translate.instant('ADMN.CORCTTYP'), 
      this.translate.instant('ADMN.ORDRTYP'), this.translate.instant('ADMN.ORDRPRORTY'),
      this.translate.instant('ADMN.ORDRSTUS'), this.translate.instant('ADMN.ORDRPROLMS'), this.translate.instant('ADMN.ORDRPROGRS')];
  
    this.getUserDataFlag = false;
    this.activeTab = 'data';
    this.getData(this.translate.instant('ADMN.ORDRTYP'));
    });
  }

  setActiveTab(tabName) {
    this.activeTab = tabName;
    if (tabName == 'users') {
      this.getUserDataFlag = true;
    } else if (tabName == 'upload') {
      this.getUserDataFlag = false;
    } else if (tabName == 'assign-tec') {
      this.getUserDataFlag = false;
    } else if (tabName == 'settingDispatchers') {
      this.getUserDataFlag = false;
    } else if (tabName == 'data') {
      this.getUserDataFlag = false;
    }
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  getData(type) {
    this.getDataForType = type;
    console.log(this.getDataForType);
  }

}
