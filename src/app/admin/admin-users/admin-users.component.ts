import {Component, OnInit, OnDestroy, Input, OnChanges} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
import {MessageService} from 'primeng/components/common/messageservice';
import {AddEditUserModalComponent} from "./add-edit-modal/add-edit-modal.component"
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnDestroy, OnChanges {
  @Input() getData: boolean;
  users: any[];
  UsersSubscription: Subscription;
  modalRef: any;
  toggleLoading;

  constructor(private lookup: LookupService, private messageService: MessageService, private modalService: NgbModal,
    private translate: TranslateService) {
  }

  // ngOnInit() {
  //   this.toggleLoading = true;
  // }

  ngOnChanges() {
    this.toggleLoading = true;
    if (this.getData) {
      this.UsersSubscription = this.lookup.getUsers().subscribe((users) => {
          this.toggleLoading = false;
          this.users = users;
          this.users.map((user) => {
            user.name = `${user.firstName} ${user.middleName} ${user.lastName}`
          });
          //console.log('got users');
        },
        err => {
          this.toggleLoading = false;
          this.messageService.add({
            severity: 'error',
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkDesc')
          })
        })
    }
  }

  ngOnDestroy() {
    this.UsersSubscription && this.UsersSubscription.unsubscribe();
  }

  edit(user) {
    let userCopy = Object.assign({}, user);
    console.log(user);
    this.openModal(userCopy, `Edit ${userCopy.name} info`);
    this.modalRef.result
      .then((editedData) => {
        let userRoles = [];
        userRoles.push(editedData.roleNames);
        editedData.roleNames = userRoles;
        delete editedData.name;
        console.log(editedData);
        console.log(editedData.roleNames);
        this.lookup.updateUser(editedData).subscribe(() => {
            this.messageService.add({
              severity: 'success',
              summary: this.translate.instant('MSG.sucesful'),
              detail: `${user.name} info Edited & Saved Successfully!`
            });
            user = editedData;
          },
          err => {
            this.messageService.add({
              severity: 'error',
              summary: this.translate.instant('MSG.fail'),
              detail: `Failed to edit ${user.name} info due to server error.`
            })
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: this.translate.instant('MSG.nothingEdit'),
          detail: `No edits saved to ${user.name} info`
        })
      })
  }

  remove(user) {
    //console.log(user);
    this.lookup.deleteUser(user.userName).subscribe(() => {
        this.messageService.add({
          severity: 'success',
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkDesc')
        });
        this.users = this.users.filter((oneUser) => {
          return oneUser.userName != user.userName;
        })
      },
      err => {
        this.messageService.add({
          severity: 'error',
          summary: this.translate.instant('MSG.sucesful'),
          detail: this.translate.instant('MSG.usrDeletSuces')
        })
      })
  }

  add() {
    this.openModal({}, `Add new user`);
    this.modalRef.result
      .then((newUser) => {
        let userRoles = [];
        //console.log(newUser.roleNames);
        userRoles.push(newUser.roleNames);
        newUser.roleNames = userRoles;
        //console.log(newUser.roleNames);
        this.lookup.postNewUser(newUser).subscribe((resUser) => {
            console.log(resUser);
            this.messageService.add({
              severity: 'success',
              summary: 'saved successfully!',
              detail: `New user ${resUser.firstName} ${resUser.middleName}${resUser.lastName} saved successfully!`
            });
            resUser.name = resUser.firstName + resUser.middleName + resUser.lastName;
            this.users.push(resUser);
          },
          err => {
            this.messageService.add({
              severity: 'error',
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkNewUsrDesc')
            })
          })
      })
      .catch(() => {
        this.messageService.add({
          severity: 'info',
          summary: this.translate.instant('MSG.usrCancl'),
          detail: this.translate.instant('MSG.usrSavCancl')
        })
      })
  }

  openModal(data, header) {
    this.modalRef = this.modalService.open(AddEditUserModalComponent);
    this.modalRef.componentInstance.header = header;
    // this.modalRef.componentInstance.type = 'prompt';
    this.modalRef.componentInstance.data = data;
  }
}
