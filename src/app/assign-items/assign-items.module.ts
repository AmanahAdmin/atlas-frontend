import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule, BaseRequestOptions} from '@angular/http';
import {AssignItemsComponent} from './assign-items/assign-items.component';
import {Routes, RouterModule} from "@angular/router";
import {AuthGuardGuard} from "../api-module/guards/auth-guard.guard";
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ReleaseModalComponent} from './release-modal/release-modal.component';
import {SharedModuleModule} from "../shared-module/shared-module.module";
import {QRCodeModule} from 'angular2-qrcode';
import {GrowlModule} from 'primeng/components/growl/growl';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

const assignItemsRoutes: Routes = [
    {
        path: '',
        component: AssignItemsComponent,
        canActivate: [AuthGuardGuard],
        data: {roles: ['Dispatcher', 'Admin']}
    }
];

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '../../assets/i18n/', '.json');
  }


@NgModule({
    imports: [
        GrowlModule,
        CommonModule,
        FormsModule,
        HttpModule,
        SharedModuleModule,
        NgxDatatableModule,
        ProgressSpinnerModule,
        QRCodeModule,
        RouterModule.forChild(assignItemsRoutes),
        HttpClientModule,
        TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
        })
    ],
    entryComponents: [
        ReleaseModalComponent
    ],
    declarations: [AssignItemsComponent, ReleaseModalComponent]
})
export class AssignItemsModule {
}
