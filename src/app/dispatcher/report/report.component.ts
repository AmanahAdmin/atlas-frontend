import {Component, OnInit, ViewChild} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {OrderFilterComponent} from "../order-filter/order-filter.component";
// import {Angular2Csv} from 'angular2-csv/Angular2-csv';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})

export class ReportComponent implements OnInit {
  orders: any[] = [];
  orderProgressMode: boolean = false;
  @ViewChild(OrderFilterComponent) orderFilterComponent: OrderFilterComponent;
  lang = localStorage.getItem('lang');

  constructor(private lookup: LookupService, private auth: AuthenticationServicesService, private utilities: UtilitiesService,
    private translate: TranslateService) {
    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
  }

  applyFilter(filteredOrders) {
    this.orders = filteredOrders;
    this.orders.map((order) => {
      // if(){

      // }
      order.createdDateView = this.utilities.convertDatetoNormal(order.order.createdDate);
      if(order.order.customer.kinder_person_tel_no){
        order.phoneView = order.order.customer.kinder_person_tel_no
      }
      if(order.order.customer.name){
        order.custName = order.order.customer.name
      }
    });
    console.log(this.orderProgressMode);
    if (this.orderProgressMode) {
      this.orders.map((order) => {
        order.orderProgress && (order.orderProgress.createdDateView = this.utilities.convertDatetoNormal(order.orderProgress.createdDate));
      })
    }
  }

  applyOrderProgressMode(mode) {
    console.log(mode);
    this.orderProgressMode = mode;
  }

  getAllOrdersByDis() {
    // let dispatcherId = this.auth.CurrentUser().id;
    // this.lookup.getDisAllOrders(dispatcherId).subscribe((dis) => {
    //     this.orders = dis.orders;
    //     console.log(this.orders);
    //   },
    //   err => {
    //
    //   });
    this.orders = [];
  }

}
