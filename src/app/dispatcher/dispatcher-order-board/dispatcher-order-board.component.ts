import {Component, OnInit, OnDestroy, OnChanges, AfterViewInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {Subscription} from "rxjs";
import {DragulaService} from 'ng2-dragula';
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {BulkAssignModalComponent} from "../bulk-assign-modal/bulk-assign-modal.component";
import {HubConnection} from '@aspnet/signalr';
import {vehicleHubUrl} from '../../../app/api-module/services/globalPath'
import {EditOrderModalComponent} from "../../customer/edit-order-modal/edit-order-modal.component";
import {printOrderComponent} from "./printOrder/printOrder.component";

import * as signalR from "@aspnet/signalr";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-dispatcher-order-board',
  templateUrl: './dispatcher-order-board.component.html',
  styleUrls: ['./dispatcher-order-board.component.css']
})

export class DispatcherOrderBoardComponent implements OnInit, OnDestroy {
  toggleLoading: boolean;
  orders: any[];
  filteredOrders: any[] = [];
  orderMap: any[];
  tecs: any[] = [];
  cornerMessage: any[];
  selectedOrders: any[] = [];
  dispatcherOrdersSubscription: Subscription;
  tecsByDisSubscription: Subscription;
  orderStatusObservable: Subscription;
  allVehiclesSubscription: Subscription;
  isOrderSelected: any;
  orderToPost: any;
  tecToPost: any;
  toggleMap: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  lat: number = 29.378586;
  lng: number = 47.990341;
  zoom: number = 8;
  display: boolean;
  allVehicles: any[] = [];
  availabilityButtons: any[] = [];
  filterStatusClass: string[] = [];
  todayOrdersStatus: boolean;
  vehiclesStatus: boolean;
  currentActiveTab: number;
  notificationSubscription: Subscription;
  private _hubConnection: HubConnection;
  lang = localStorage.getItem('lang');
  seletetdTecs:any;


  constructor(private lookup: LookupService, private utilities: UtilitiesService, private dragulaService: DragulaService,
    private authService: AuthenticationServicesService, private modelService: NgbModal,
    private translate: TranslateService) {
      translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    this.currentActiveTab = 1;
    this.orderMap = [];
    this.availabilityButtons = [
      {label: 'Yes', value: true},
      {label: 'No', value: false}
    ];
    this.getDispatcherOrders();
    this.getDisAllOrders();
    this.getTecsByDis();
    this.getAllVehicles();
    this.display = true;
    this.todayOrdersStatus = true;
    this.vehiclesStatus = true;
    this.utilities.toggleFullWidth = true;
    this.filterStatusClass = ['zero-filter-width'];
    this.hubInit();
    this.dragulaService.setOptions('orders', {
      removeOnSpill: false,
      copySortSource: false,
      revertOnSpill: true,
      accepts: (item, target) => {
        return item.classList.contains('order-card') && !target.classList.contains('not-available');
      }
    });

    this.dragulaService.drop.subscribe((value) => {
      this.postOrderByTec(value.slice(1));
    });

    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
        console.log(value);
        if (value) {
          this.openViewOrderModal(value);
          let orderFoundFlag = false;
          if (this.orders) {
            this.orders.map((order, i) => {
              if (order.id == value.id) {
                orderFoundFlag = true;
              }
              if (this.orders.length == i + 1 && !orderFoundFlag) {
                this.getDispatcherOrders()
              }
            })
          } else {
            this.getDispatcherOrders();
          }
        }
      },
      err => {
        console.log(err);
      })

  }

  openViewOrderModal(notificationOrder) {
    let editOrderModalRef = this.modelService.open(EditOrderModalComponent);
    editOrderModalRef.componentInstance.data = notificationOrder;
    editOrderModalRef.componentInstance.notificationOrder = true;
  }

  printOrder() {
    let printOrderModalRef = this.modelService.open(printOrderComponent);
  }

  ngOnDestroy() {
    this.dragulaService.destroy('orders');
    this.utilities.toggleFullWidth = false;
    this.dispatcherOrdersSubscription && this.dispatcherOrdersSubscription.unsubscribe();
    this.tecsByDisSubscription && this.tecsByDisSubscription.unsubscribe();
    this.allVehiclesSubscription && this.allVehiclesSubscription.unsubscribe();
    this.orderStatusObservable && this.orderStatusObservable.unsubscribe();
    this.utilities.setSavedNotificationText('');
    this.notificationSubscription && this.notificationSubscription.unsubscribe();
  }

  hubInit() {
    let roles = this.authService.CurrentUser().roles;
    let userId = this.authService.CurrentUser().id;
    let token = this.authService.CurrentUser().token.accessToken;
    console.log(this.authService.CurrentUser())


    // const options = { logger: signalR.LogLevel.Trace, transport: signalR.HttpTransportType.WebSockets }
    // const connection = new signalR.HubConnectionBuilder()
    // .withUrl(vehicleHubUrl(userId, roles, token),options)
    // .build();

    // connection.start().catch(err => document.write(err));


    // connection.on('vehicleHubUrl', (userId:string, roles:any, token:string)=>{
    //   this.hubStart();
    // });

    this._hubConnection = new HubConnection(vehicleHubUrl(userId, roles, token));
    this.hubStart();
    
    // let connection = new signalR.HubConnection(vehicleHubUrl(userId, roles, token));
 
    // connection.on('send', data => {
    //     console.log(data);
    // });
    
    // connection.start()
    //     .then(() => connection.invoke('send', 'Hello'));
  }

  hubStart() {
    this._hubConnection.start()
      .then(() => {
        console.log('started');
        this._hubConnection.on('onVehicleAddOrUpdate', (data: any) => {
          let foundFlag = false;
          console.log(data);
          this.allVehicles.map((veh, i) => {
            data.map((newCar) => {
              if (veh.id == newCar.id) {
                console.log('found');
                this.allVehicles[i] = newCar;
                foundFlag = true;
              } else if (!foundFlag && (this.allVehicles.length - 1 == i)) {
                this.allVehicles.push(newCar)
              }
            })
          });
          console.log(this.allVehicles);
        })
      })
      .catch(() => {
        console.log('failed');
      })
  }

  orderTransfered(id) {
    console.log(id);
    this.orders = this.orders.filter((order) => {
      return id != order.id;
    });
    console.log(id);
    this.tecs.map((tec) => {
      tec.orders = tec.orders.filter((order) => {
        return id != order.id;
      })
    });
  }

  openBulkAssignModal() {
    let modalRef = this.modelService.open(BulkAssignModalComponent);
    modalRef.componentInstance.selectedOrders = this.selectedOrders;
    modalRef.result
      .then((transferedOrders) => {
        console.log(transferedOrders);
        this.orders = this.orders.filter((order) => {
          return !transferedOrders.includes(order.id);
        });
        this.tecs.map((tec) => {
          tec.orders = tec.orders.filter((order) => {
            return !transferedOrders.includes(order.id);
          })
        });
        this.selectedOrders = [];
      })
      .catch(() => {

      })
  }

  addOrderToSelection(order) {
    if (!this.selectedOrders.includes(order.id)) {
      this.selectedOrders.push(order.id);
    } else {
      this.selectedOrders = this.selectedOrders.filter((singleOrderId) => {
        return order.id != singleOrderId;
      })
    }
    console.log(this.selectedOrders);
  }

  toggleBulkAssign() {
    if (!this.isBulkAssign) {
      this.selectedOrders = [];
    }
  }

  cancelBulkAssignMode() {
    this.isBulkAssign = false;
    this.selectedOrders = [];
    this.isOrderSelected = false;
  }

  toggleActiveTab(activeTab) {
    this.currentActiveTab = activeTab;
  }

  toggleTodayOrders() {
    this.todayOrdersStatus = !this.todayOrdersStatus;
    if (this.todayOrdersStatus) {
      this.getDisAllOrders();
    } else {
      this.orderMap = [];
    }
  }

  toggleVehiclesStatus() {
    this.vehiclesStatus = !this.vehiclesStatus;
    if (this.vehiclesStatus) {
      this.getAllVehicles();
    } else {
      this.allVehicles = [];
    }
  }

  applyFilterMap(filteredOrders) {
    this.orderMap = filteredOrders;
    console.log('orderMap' + this.orderMap)
    this.orderMap.map((order) => {
      if (order.orderStatus) {
        order.statusUrl = 'assets/Images/' + order.orderStatus.id + '.png';
      }
    });
    this.todayOrdersStatus = false;
  }

  private _filterOrdersListBoard(list, terms) {
    console.log(terms);
    let termsCount = 0
      , filterOrders = [];

    for (let property in terms) {
      if (terms.hasOwnProperty(property)) {
        termsCount++;
      }
    }

    list.map((order) => {
      let existedTermsCount = 0;
      console.log(order);
      if (terms.orderNo == order.code) {
        existedTermsCount++;
      }
      if (terms.status) {
        terms.status.map((state) => {
          if (state.id == order.orderStatus.id) {
            existedTermsCount++;
          }
        })
      }
      if (terms.problems) {
        terms.problems.map((problem) => {
          if (problem.id == order.orderProblem.id) {
            existedTermsCount++;
          }
        })
      }
      console.log(new Date(terms.startDateFrom) < new Date(order.createdDate));
      if (terms.startDateFrom) {
        if (new Date(terms.startDateFrom) <= new Date(order.createdDate)) {
          existedTermsCount++;
        }
      }
      if (terms.startDateTo) {
        if (new Date(terms.startDateTo) >= new Date(order.createdDate)) {
          existedTermsCount++;
        }
      }
      if (existedTermsCount == termsCount) {
        filterOrders.push(order);
      }
    });
    return filterOrders;
  }

  applyFilterBoard(terms) {
    this.tecs.map((tec) => {
      tec.filterOrders = this._filterOrdersListBoard(tec.orders, terms);
    });
    this.filteredOrders = this._filterOrdersListBoard(this.orders, terms)
  }

  resetBoardFilter() {
    this.filteredOrders = this.orders.slice();
    this.tecs.map((tec) => {
      tec.filterOrders = tec.orders.slice();
    })
  }

  activeOrder(order) {
    console.log(order);
    if (this.toggleMap) {
      this.lat = order.location.latitude;
      this.lng = order.location.longitude;
      this.zoom = 16
    }
  }

  getAllVehicles() {
    this.allVehiclesSubscription = this.lookup.getAllVehicles().subscribe((vehicles) => {
        this.allVehicles = vehicles.returnData;
        console.log(this.allVehicles);
      },
      err => {
        console.log('failed');
      })
  }

  toggleFilter(status) {
    console.log(status);
    if (status) {
      this.filterStatusClass = ['full-filter-width'];
    } else {
      this.filterStatusClass = ['zero-filter-width'];
    }
  }

  applyToggleMap() {
    if (this.toggleMap) {
      this.toggleOverflowX = ['hide-overflowX']
    } else {
      this.toggleOverflowX = ['show-overflowX']
    }
  }

  setActiveOrder(order) {
    // console.log('order selected');
    this.orderToPost = order;
  }

  setActiveTec(tec) {
    // console.log('tec selected');
    this.tecToPost = tec;
  }

  toggleAvailabilityClass(tec) {
    if (!tec.isAvailable) {
      return ['not-available']
    }
  }

  postTecAvailability(tec) {
    console.log(tec);
    // console.log(e);
    // e.preventDefault();
    if (!tec.orders.length) {
      let availability = {
        id: tec.id,
        isAvailable: tec.isAvailable
      };
      console.log(availability);
      this.lookup.postTecAvailability(availability)
        .subscribe(() => {
            console.log('success')
          },
          err => {

          })
    } else {
      tec.isAvailable = true;
      // e.checked = true;
      // console.log(e);
    }
  }

  checkTecOrders(tec) {
    return !!tec.filterOrders.length;
  }

  private _moveOrdersBetweenSections(orderId, tecId) {
    let selectedOrder;
    this.orders.map((order) => {
      if (order.id == orderId) {
        selectedOrder = order;
      }
    });
    this.tecs.map((tec) => {
      return tec.filterOrders.map((order) => {
        if (order.id == orderId) {
          selectedOrder = order;
        }
      });
    });

    console.log(selectedOrder);

    if (tecId) {
      this.tecs.map((tec) => {
        if (tec.id == tecId) {
          tec.filterOrders.splice(tec.filterOrders.indexOf(selectedOrder), 1)
        }
      })
    } else {
      this.orders.splice(this.orders.indexOf(selectedOrder), 1)
    }
  }

  postOrderByTec(val) {
    setTimeout(() => {
      let orderByTec = {
        fK_technican_id: this.tecToPost.id,
        fK_order_id: this.orderToPost.id,
        // isTransfer: false
      };
      console.log(this.orderToPost);
      console.log(orderByTec);
      // console.log(val);
      if (val[1].classList.contains('single-tec-overflow')) {
        this.lookup.postOrderToTec(orderByTec).subscribe((res) => {
            console.log(res);
            console.log('success');
            this.getDispatcherOrders();
            this.getTecsByDis();
          },
          err => {
            console.log('fail');
          })
      }
      else if (val[1].classList.contains('orders-overflow-container')) {
        this.lookup.postUnAssignedOrder(this.orderToPost.id).subscribe((res) => {
            console.log(res);
            // this.orderToPost.fK_Technican_Id = null;
            console.log('success');
            this.getDispatcherOrders();
            this.getTecsByDis();
          },
          err => {
            console.log('fail');
          })
      }
    }, 200);

  }

  getDispatcherOrders() {
    this.toggleLoading = true;
    let dispatcherId = this.authService.CurrentUser().id;
    this.dispatcherOrdersSubscription = this.lookup.getDispatcherOrders(dispatcherId).subscribe((dispatcher) => {
        this.orders = dispatcher.orders;
        console.log("this.orders");
        console.log(this.orders);
        this.orders.map((order) => {
          order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
          order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
        });
        this.filteredOrders = this.orders.slice();
        // this.orderMap = this.orders.slice();
        console.log('filteredOrders' + JSON.stringify(this.filteredOrders));
        this.toggleLoading = false;
      },
      err => {
        this.toggleLoading = false;
      })
  }

  getTecsByDis() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.tecsByDisSubscription = this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
        this.tecs = tecs;
        console.log(JSON.stringify(this.tecs))
        this.tecs.map((tec) => {
          tec.filterOrders = tec.orders.slice();
          console.log('filterOrders' +JSON.stringify(tec.filterOrders))
          tec.orders.map((order) => {
            order.startDateView = this.utilities.convertDatetoNormal(order.startDate);
            order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
          })
        });
        console.log(tecs);
      },
      err => {
      })
  }

  getDisAllOrders() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.todayOrdersStatus = true;
    this.lat = 29.378586;
    this.lng = 47.990341;
    this.zoom = 8;

    this.lookup.getDisAllOrders(dispatcherId).subscribe((orderMap) => {
        this.orderMap = orderMap.orders;
        console.log(dispatcherId)
        console.log(this.orderMap);
        this.orderMap.map((order) => {
          if (order.orderStatus) {
            order.statusUrl = 'assets/Images/' + order.orderStatus.id + '.png';
          }
        })
      },
      err => {
        console.log('failed');
      })
  }

}
