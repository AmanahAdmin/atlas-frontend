import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DispatcherOrderBoardComponent} from './dispatcher-order-board/dispatcher-order-board.component';
import {RouterModule} from '@angular/router';
import {dispatcherRoutes} from './dispatcher-routes'
import {SharedModuleModule} from '../shared-module/shared-module.module';
import {OrderCardComponent} from './order-card/order-card.component';
import {SetTimeModalComponent} from './set-time-modal/set-time-modal.component';
import {TransferDispatcherModalComponent} from './transfer-dispatcher-modal/transfer-dispatcher-modal.component';
import {OrderProgressModalComponent} from './order-progress-modal/order-progress-modal.component';
import {AgmCoreModule} from '@agm/core';
import {API_Key} from '../api-module/services/globalPath'
import {SidebarModule} from 'primeng/components/sidebar/sidebar';
import {ReportComponent} from './report/report.component';
import {OrderFilterComponent} from './order-filter/order-filter.component';
import {VisitTimeCalenderComponent} from './visit-time-calender/visit-time-calender.component';
// import {ScheduleModule} from 'primeng/components/schedule/schedule';
import {OrderDetailsModalComponent} from './order-details-modal/order-details-modal.component';
import {ToggleButtonModule} from 'primeng/togglebutton';
import { BulkAssignModalComponent } from './bulk-assign-modal/bulk-assign-modal.component';
import {PreventiveComponent} from './preventive/preventive/preventive.component';
import {PreventiveDetailsModalComponent} from './preventive/preventive-details-modal/preventive-details-modal.component';
// import {RadioButtonModule} from 'primeng/primeng';
import {HttpClient, HttpClientModule} from "@angular/common/http";

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import * as jQuery from 'jquery';
import { AutoCompleteModule, ScheduleModule, DialogModule, CalendarModule, DropdownModule } from 'primeng/primeng';
(window as any).jQuery = (window as any).$ = jQuery; 
import 'fullcalendar';

import {printOrderComponent} from './dispatcher-order-board/printOrder/printOrder.component'

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    SidebarModule,
    ScheduleModule,
    ToggleButtonModule,
    // RadioButtonModule,
    DropdownModule,
    AutoCompleteModule,
    CalendarModule,
    HttpClientModule,
    DialogModule,
    AgmCoreModule.forRoot({
      apiKey: API_Key
    }),
    RouterModule.forChild(dispatcherRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  entryComponents: [
    SetTimeModalComponent,
    TransferDispatcherModalComponent,
    OrderProgressModalComponent,
    OrderDetailsModalComponent,
    BulkAssignModalComponent,
    PreventiveComponent,
    PreventiveDetailsModalComponent,
    printOrderComponent
  ],
  declarations: [DispatcherOrderBoardComponent, OrderCardComponent, SetTimeModalComponent, TransferDispatcherModalComponent, OrderProgressModalComponent, ReportComponent, 
    OrderFilterComponent, VisitTimeCalenderComponent, OrderDetailsModalComponent, BulkAssignModalComponent, PreventiveComponent , PreventiveDetailsModalComponent,printOrderComponent ]
})

export class DispatcherModule {
}
