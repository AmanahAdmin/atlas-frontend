import {CustomerHistory, customer, contract, complain, WorkOrderDetails} from './../../models/customer-model';
import {Observable} from 'rxjs';
import {newCallDetails} from './../../models/newCustomer';
import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import * as myGlobals from '../globalPath';

@Injectable()
export class CustomerCrudService {

  constructor(private http: Http) {
  }

  GoSearchCustomer(SearchField: string) {
    return this.http.get(myGlobals.SearchCustomer + `${SearchField}`, this.jwt()).map((response: Response) => response.json());
  }

  addCustomer(customer: any) {
    return this.http.post(myGlobals.CreatenewCustomer, customer, this.jwt()).map((response: Response) => response.json());
  }

  GetCallDetail(Id: number): Observable<CustomerHistory> {
    return this.http.get(myGlobals.GetCallDetails + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let bearercurrentUser = 'Bearer '+ currentUser.token.accessToken;
    //console.log(currentUser);
    if (currentUser && currentUser.token.accessToken) {
      let headers = new Headers({'Authorization': bearercurrentUser});
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({headers: headers});
    }
  }
}
