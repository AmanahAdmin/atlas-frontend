import {AuthGuardGuard} from './api-module/guards/auth-guard.guard';
import {NgModule, ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [

  {path: '', loadChildren: './login-register-module/login-register-module.module#LoginRegisterModuleModule'},
  {path: 'search', loadChildren: './customer/customer.module#CustomerModule'},
  {path: 'admin', loadChildren: './admin/admin.module#AdminModule'}
  ,
  {
    path: 'dispatcher', loadChildren: './dispatcher/dispatcher.module#DispatcherModule'
  },
  {
    path: 'assign-item', loadChildren: './assign-items/assign-items.module#AssignItemsModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class RoutingModule {
}

export const RoutingComponents = [];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
